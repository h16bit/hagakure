﻿/**
 * Hagakure todo module
 */
(function () {

    hagakure = {};

    hagakure.appControl = (function () {
        var controls = [],
            callbacks = [];
        return {
            init: function (itemsElementPanelName, listElementPanelName, textInputName, textAddButtonName) {
                hagakure.appControl.addControl(hagakure.formControl);
                hagakure.formControl.bindToDomElement(itemsElementPanelName, textInputName, textAddButtonName);

                hagakure.appControl.addControl(hagakure.itemsControl);
                hagakure.itemsControl.bindToDomElement(listElementPanelName);
            },
            raiseEvent: function (eventName, p1, p2, p3, p4, p5) {
                for (var i = 0; i < controls.length; i++) {
                    if (controls[i][eventName]) {
                        controls[i][eventName](p1, p2, p3, p4, p5);
                    }
                }
                for (var j = 0; j < callbacks.length; j++) {
                    if (callbacks[j].eventName == eventName) {
                        callbacks[j](p1, p2, p3, p4, p5);
                    }
                }
            },
            subscribeEvent: function(eventName, callback) {
                callback.eventName = eventName;
                callbacks.push(callback);
            },
            unsubscribeEvent: function(callback) {
                var eventIndex = callbacks.indexOf(callback);
                callbacks.splice(eventIndex, 1);
            },
            addControl: function (control) {
                controls.push(control);
            },
            removeControl: function (control) {
                var controlIndex = controls.indexOf(control);
                controls.splice(controlIndex, 1);
            }
        }
    })();


    hagakure.formControl = (function () {
        var controlDomElement,
            textInputElement,
            textAddButtonElement;
        return {
            bindToDomElement: function (domElementId, textInputElementId, textAddButton) {
                controlDomElement = document.getElementById(domElementId);
                textInputElement = document.getElementById(textInputElementId);
                textAddButtonElement = document.getElementById(textAddButton);

                textAddButtonElement.addEventListener("click", function () {
                    if (textInputElement.value) {
                        var newId = "id-" + new Date().getTime().toString();
                        hagakure.appControl.raiseEvent("addItem", newId, textInputElement.value);
                        textInputElement.value = "";
                    }
                });
            }
        }
    })();

    hagakure.itemsControl = (function () {
        var items = [],
            controlDomElementName;
        return {
            bindToDomElement: function (domElementId) {
                controlDomElementName = domElementId;
            },
            addItem: function (id, text) {
                items.push({ id: id, text: text});
                DOMC.createItem(text, { 
                    parentId: controlDomElementName,
                    tag: 'div',
                    event: {
                        type: 'click',
                        target: 'a',
                        call: (function (id) {
                            return function () {
                                hagakure.appControl.raiseEvent("removeItem", id.toString());
                            }
                        }(id))
                    },
                    id: id
                });
            },
            removeItem: function (id) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].id === id) {
                        items.splice(i, 1);
                        DOMC.deleteItem(id);
                        break;
                    }
                }
            },
            loadItems: function () {
                var itemsToLoad = DOMC.getData();

                for (var i = 0; i < itemsToLoad.length; i++) {
                    var itemToLoad = itemsToLoad[i];

                    items.push({ id: i.toString(), text: itemToLoad });
                    DOMC.createItem(itemToLoad, {
                        parentId: controlDomElementName,
                        tag: 'div',
                        event: {
                            type: 'click',
                            target: 'a',
                            call: (function (i) {
                                return function () {
                                    hagakure.appControl.raiseEvent("removeItem", i.toString());
                                }
                            }(i))
                        },
                        id: i.toString()
                    });
                }
            },
            saveItems: function () {
                var itemsToSave = items.map(function (item) { return item.text; });

                DOMC.setData(itemsToSave);
            }
        }
    })();


    
    

})();