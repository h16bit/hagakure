/*
 * Module for creating items in DOM
 *
 * createItem(text for item, properties):
 * properties: {
 *     tag: 'div',              //Its tag that will be using for element creation
 *     parentId: 'list',        //Id of parent container
 *     event: {
 *         type: 'onClick',     //Type of event
 *         call: function(){},  //Callback that will be calling on event
 *         target: 'a'          //Tag of children element, which will be evented
 *     }
 * }
*/
(function(global){
    var DOMC = function(){};

    //Set default template for new element
    //==============================================================
    DOMC.prototype.template = '<div class="item-content">' +
                                    '{#text}' +
                                 '</div>' +
                                 '<a>×</a>';

    //Function for creating new item
    //==============================================================
    DOMC.prototype.createItem = function(text,properties){
        var re = /{[^{}]+}/g;

        //Create new DOM element
        var ne = document.createElement(properties.tag);
        var parent = document.getElementById(properties.parentId);

        //Insert text
        ne.innerHTML = this.template.replace(re,text);

        //Set default properties
        ne.className = 'item';

        //Attach event if it have
        if (properties.event != undefined){
            switch(typeof properties.event.target){
                case 'string':
                    var eef = ne.getElementsByTagName(properties.event.target)[0];
                    eef.addEventListener(properties.event.type,properties.event.call);
                    break;
                default:
                    ne.addEventListener(properties.event.type,properties.event.call);
                    break;
            }
            delete properties.event;
        }

        //Delete system options
        delete properties.tag;
        delete properties.parentId;

        //Set attributes for new element
        for(var i in properties){
            ne.setAttribute(i,properties[i]);
        }

        //Add to the parent element
        parent.appendChild(ne);
    }

    //Delete element
    //==============================================================
    DOMC.prototype.deleteItem = function(el){
        switch(typeof el){
            case 'object':
                el.parentNode.removeChild(el);
                break;
            case 'string':
                var del = document.getElementById(el);
                this.deleteItem(del);
                break;
            default:
                break;
        }
    }

    //Get elements by attribute
    //==============================================================
    DOMC.prototype.getElements = function(selector){
        var matchingElements = [];
        var allElements = document.getElementsByTagName('*');

        //Go by all DOM tree
        for (var i = 0,l=allElements.length; i < l; i++)
        {
            //If we found element with needed attribute
            if (allElements[i].getAttribute(selector))
            {
                // Element exists with attribute. Add to array.
                matchingElements.push(allElements[i]);
            }
        }
        return matchingElements;
    };

    //Save data to storage
    DOMC.prototype.setData = function(data){
        var dt = {
            date: new Date().getDate(),
            data: data
        };

        localStorage.setItem('hagakure',JSON.stringify(dt));
    };

    //Get data from storage
    DOMC.prototype.getData = function(){
        var dt = JSON.parse(localStorage.getItem('hagakure'));

        return dt && dt.data ? dt.data : [];
        //return (dt != null && dt.data != null && dt.date != null && dt.date == new Date().getDate()) ? dt.data : [];
    };

    //Initialize module
    //==============================================================
    global.DOMC = new DOMC();

})(window);