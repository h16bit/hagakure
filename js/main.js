/*
 * Main Module
*/
(function () {

    hagakure.appControl.init("panel", "list", "textInput", "textAddButton");
    hagakure.appControl.raiseEvent("loadItems");

    hagakure.appControl.subscribeEvent("addItem", function () {
        hagakure.appControl.raiseEvent("saveItems");
    });

    hagakure.appControl.subscribeEvent("removeItem", function () {
        hagakure.appControl.raiseEvent("saveItems");
    });

})();